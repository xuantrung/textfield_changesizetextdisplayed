//
//  ViewController.swift
//  notification_TextField
//
//  Created by Admin on 3/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var textField: UITextField!
    //var label : UILabel!

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField.delegate = self
        textField.rightView = label
        textField.rightViewMode = .never
        //addLabel()
    }

    func addLabel(){
        //create label
//        label = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
//        label.text = "test thử xem sao"
//        label.textColor = UIColor.red
//        label.textAlignment = .left
//        textField.rightView = label
//        textField.rightViewMode = .never
    }
    @IBAction func clickButton(_ sender: Any) {
        guard let text = textField.text else {
            return
        }
        if text.count > 30{ //điều kiện này là so sánh với username, password trên server
            textField.rightViewMode = .always
        }
    }

}
extension ViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.rightViewMode = .never
        print("begin")
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("return")
        textField.resignFirstResponder() //hidden keyboard when clicked return
        return true
    }


}

